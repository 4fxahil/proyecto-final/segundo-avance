from django.http import JsonResponse
from decimal import FloatOperation
from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import get_object_or_404, render, redirect
from django.db import transaction
from django.utils.crypto import get_random_string
from django.views import generic
from django.urls import reverse_lazy

# Create your views here.

from core.models import Product, ItemPedido, Pedido
from .carrito import Carrito


def corder(request):
    carrito = Carrito(request)

    data = []
    for item in carrito:
        product = item['product']
        stock = product.stock
        qty_range = range(1, product.stock + 1)
        data.append({'item': item, 'qty_range': qty_range})
        #print(f'Item: {item}, Qty Range: {list(qty_range)}')
        
    #print(f'data: {data}')
    return render(request, 'carrito/corder.html', {'data': data})

 
def carrito_add(request):

    carrito = Carrito(request)

    if request.method == 'POST' and request.POST.get('action') == 'post':
        try:
            product_id = int(request.POST.get('productId'))
            product_qty = int(request.POST.get('productQty'))
        except (ValueError, TypeError):
            # Manejar errores si 'productId' o 'productQty' no son números enteros
            return JsonResponse({'error': 'Invalid data'}, status=400)

        try:
            product = Product.objects.get(id=product_id)
        except Product.DoesNotExist:
            return JsonResponse({'error': 'Product not found'}, status=404)

        # Asegura de que el producto y la cantidad sean válidos antes de agregar al carrito
        if product and product_qty > 0:
            carrito.add(product=product, qty=product_qty)
            
            carrito_qty = carrito.__len__()
            response = JsonResponse({'qty': carrito_qty})
            return response

    # si no es una solicitud POST válida
    return JsonResponse({'error': 'Invalid request'}, status=400)



def DeleteCarrito(request):
    carrito = Carrito(request)
    if request.POST.get('action') == 'post':
        product_id = int(request.POST.get('productId'))
        carrito.delete(product=product_id)

        carritoQty = carrito.__len__()
        carritoTotal = carrito.get_total_price()
        response = JsonResponse({'qty': carritoQty, 'subtotal': carritoTotal})
        return response


def UpdateCarrito(request):
    carrito = Carrito(request)
    if request.POST.get('action') == 'post':
        product_id = int(request.POST.get('productId'))
        product_qty = int(request.POST.get('productQty'))
        carrito.update(product=product_id, qty=product_qty)

        carritoQty = carrito.__len__()
        carritoTotal = carrito.get_total_price()
        response = JsonResponse({'qty': carritoQty, 'subtotal': carritoTotal})
        return response



class pagoos(LoginRequiredMixin,generic.View):
    template_name = "carrito/pago.html"
    context={}
    login_url = reverse_lazy("home:acceso")
    def get(self, request):
        self.context = {
        }
        return render(request, self.template_name, self.context)
    def post(self, request):

        carrito = Carrito(request)

            # Procesar el formulario y realizar el pago (puede ser una integración con un proveedor de pagos)
            # Si el pago es exitoso, guarda la orden en la base de datos
        with transaction.atomic():
                # Crea un nuevo pedido en la base de datos
            pedido = Pedido.objects.create(usuario=request.user, codigo_pedido=get_random_string(length=8))

                # Añade los productos al pedido
            for product_id, item in carrito.carrito.items():
                product = Product.objects.get(id=int(product_id))
                cantidad = item['qty']
                codigo_producto = get_random_string(length=8)

                ItemPedido.objects.create(pedido=pedido, product=product, cantidad=cantidad, codigo_producto=codigo_producto)

                    # Actualiza el stock del producto
                product.stock -= cantidad
            product.save()

            # Limpia el carrito después de procesar la orden
            carrito.carrito.clear()
            carrito.save()

            # Renderiza la página de confirmación
            return render(request, 'carrito/pedido.html', {'pedido': pedido})
        

class verPedidos(LoginRequiredMixin, generic.View):
    template_name="carrito/verpedidos.html"
    context={}
    login_url = reverse_lazy("home:acceso")
    def get(self, request):
        pedidos=Pedido.objects.filter(usuario=request.user)
        self.context={
            "pedidos":pedidos
        }
        return render(request, self.template_name, self.context)
    